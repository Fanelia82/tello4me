//npm init
//npm install nodemon

//  "scripts": {
//    "develop": "node app.js"
//oppure
//    "develop": "nodemon app.js"
//  },



//run with
//npm run develop


//npm install quando hai appena scaricato da git

//gulp per css e html


function sayHello(name){
	console.log("hello "+ name)
}

sayHello("Nico")

const http = require("http")
const path = require("path")
const express = require('express') // using express
const socketIO = require('socket.io')
const fs = require("fs")

let app = express()

const server_node = http.createServer((req,res)=>{
	if (req.url === "/"){
		//metto un file con fh e path module
		fs.readFile(path.join(__dirname,"app.html"),(err, data)=>{
			res.writeHead(200, {"Content-Type": "text/html"})
			res.write(data)
			res.end()
		})

		//200 == tutto ok
		//res.writeHead(200, {"Content-Type":"text/html"})
		//res.write("Hello Nico")
		//html
		//res.write("<h1>Welcome Nico ciao</h1>")
		//res.write("<h1>Welcome Nico </h1>")
		//res.write("<h1>Welcome Nico </h1>")
		//res.end()

		//req sono le request e ascolta gli eventi della pagina
	}

	if (req.url === "/app.css"){
		//metto un file con fh e path module
		fs.readFile(path.join(__dirname,"app.css"),(err, data)=>{
			res.writeHead(200, {"Content-Type": "text/css"})
			res.write(data)
			res.end()
		})
	}

	if (req.url === "/app.js"){
		//metto un file con fh e path module
		fs.readFile(path.join(__dirname,"app.js"),(err, data)=>{
			res.writeHead(200, {"Content-Type": "text/javascript"})
			res.write(data)
			res.end()
		})
	}
})

let io = socketIO(server_node)

// make connection with user from server side
io.on('connection', (socket)=>{
	console.log('New user connected')
	//emit message from server to user
	socket.emit('newMessage', {
		from:'nodejs',
		text:'Hello',
		createdAt:123
	})

	// listen for message from user
	socket.on('createMessage', (newMessage)=>{
		//console.log('newMessage', newMessage)
		var dataJSON = JSON.parse(newMessage.text)
		console.log(dataJSON)
		Send_Message_To_Drone(dataJSON)
	});
	// when server disconnects from user
	socket.on('disconnect', ()=>{
		console.log('disconnected from user')
	})
})

server_node.listen(3000, ()=>console.log("Server is running"))


// make a connection with the user from server side in order to send messages
io.on('connection', (socket)=>{
	console.log('New user connected');
});

/* TELLO SCRIPT */

//var http = require('http');
//var fs = require('fs');
var url = require('url')
//const wait = require('waait') //*??
const dgram = require('dgram')


var PORT = 8889;
var HOST = '192.168.10.1';

var commandDelays = {
	command: 500,
	takeoff: 5000,
	land: 5000,
	up: 7000,
	down:7000,
	left:5000,
	go:7000,
	right:5000,
	forward:5000,
	back:5000,
	cw:5000,
	ccw:5000,
	flip:3000,
	speed:3000,
	'battery?':500,
	'speed?':500,
	'time?':500,
}

//se in un altro file
//module.exports = commandDelays
//e lo richiamo con

//const commandDelays = require("./nomemodulefile")


const drone = dgram.createSocket('udp4')
drone.bind(PORT)
const droneState = dgram.createSocket('udp4')
droneState.bind(8890)

drone.on('message', message =>{
	console.log("Tello :"+message)
})

droneState.on('message', message =>{
	//console.log("Tello tell:"+message)
})

function handleHerror(err){
	if(err){
		console.log("ERROR")
		console.log(err)
	}
}

var blockCmd = ['emergency','rc','stop']  // 阻塞指令
var notBlockCmd = ['reset_all']

drone.send("command",0,7,PORT , HOST,handleHerror)
drone.send("battery?",0,8,PORT , HOST,handleHerror)

//main buttons

function Take_off(){
	//sendCmd('command');
	//sendCmd('mon');
	//sendCmd('takeoff');
	var delay = commandDelays["command"]
	drone.send("command",0,7,PORT , HOST,handleHerror)
	drone.send("takeoff",0,7,PORT , HOST,handleHerror)
	//client.send("command",0,7,PORT , HOST,handleHerror)
}

async function  Land(){
	//sendCmd('land');
	drone.send("land",0,4,PORT , HOST,handleHerror)
}

function Video(){
	//drone.send("land",0,4,PORT , HOST,handleHerror)
}
function Photo(){
	//drone.send("land",0,4,PORT , HOST,handleHerror)
}



//movements buttons

async function Send_Message_To_Drone(dataJSON){
	var command = dataJSON.command
	var value = dataJSON.value

	//console.log(command)
	//console.log(value)
	var cm = value*100
	//console.log(cm)


	switch (command) {
		case "api/takeoff":
			//metto un file con fh e path module
			console.log("request take off")
			Take_off()
		break
		case "api/land":
			//metto un file con fh e path module
			console.log("request land")
			Land()
		break
		case "api/video":
			//metto un file con fh e path module
			console.log("request video")
			Video()
		break
		case "api/photo":
			//metto un file con fh e path module
			console.log("request photo")
			Photo()
		break
		case "api/Up":
			var string = "up "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/Down":
			var string = "down "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/Left":
			var string = "left "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/Right":
			var string = "right "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/FW":
			var string = "forward "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/BW":
			var string = "back "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/TLeft":
			var string = "ccw "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/TRight":
			var string = "cw "+cm
			drone.send(string,0,string.length,PORT , HOST,handleHerror)
		break
		case "api/FlipF":
			drone.send("flip f",0,6,PORT , HOST,handleHerror)
		break
		case "api/FlipB":
			drone.send("flip b",0,6,PORT , HOST,handleHerror)
		break
		case "api/FlipR":
			drone.send("flip r",0,6,PORT , HOST,handleHerror)
		break
		case "api/FlipL":
			drone.send("flip l",0,6,PORT , HOST,handleHerror)
		break
		default:
		console.log(`Sorry, we are out of ${command}.`);
	}
}
