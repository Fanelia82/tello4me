var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload

// Save a reference to the `reload` method

// Watch scss AND html files, doing different things with each.
gulp.task('serve', function () {
// Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./",
        },
        startPath: "/app.html",
        host: 'localhost',
        port : 3000
    });
    gulp.watch("*.html").on("change", reload);
    gulp.watch("*.css").on("change", reload);
    gulp.watch("*.js").on("change", reload);
})

//gulp.task('default',['serve'])

gulp.task('default', gulp.series('serve', function(){
    //include the code for your default task
}));
