//connecting sockets
var socket=io()
// make connection with server from user side
socket.on('connect', function(){
	console.log('Connected to Server')
})

// message listener from server
socket.on('newMessage', function(message){
	console.log(message);
});

// when disconnected from server
socket.on('disconnect', function(){
	console.log('Disconnect from server')
})

function Take_off(){
	//verify if already in air !!! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
	/*MERDA PURA
	var xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//document.getElementById("demo").innerHTML = this.responseText;
			console.log("sending request")
		}
	}
	xhr.open('POST', 'api/takeoff')
	xhr.send()*/
	send_req("api/takeoff",0)
}

function  Land(){
	/*MERDA PURA
	var xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//document.getElementById("demo").innerHTML = this.responseText;
			console.log("sending request")
		}
	}
	xhr.open('POST', 'api/land')
	xhr.send()*/
	send_req("api/land",0)
}

var recording = false
function Record_video(){
	/*MERDA PURA
	var xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//document.getElementById("demo").innerHTML = this.responseText;
			console.log("sending request")
		}
	}
	xhr.open('POST', 'api/video')
	xhr.send()
	*/
	send_req("api/video",0)
}


function Record_photo(){
	send_req("api/photo")
}

function send_req(command,value=0){
	var params = {
		command: command,
		value: value
	}

	var string = JSON.stringify(params)
	/*//funziona col culo
	var xhr = new XMLHttpRequest()
	xhr.open('POST', "api/command")
	xhr.setRequestHeader('Content-Type', 'application/json')
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//document.getElementById("demo").innerHTML = this.responseText;
			//console.log("sending request")
		}
	}

	console.log(params)
	xhr.send(string)
	//*/

	//socket
	//socket.on('createMessage', (newMessage)=>{
	//	console.log('newMessage', newMessage);
	//})
	//console.log(params)
	socket.emit('createMessage', {
		from:'Nico',
		text:string,
	})
}

function Up(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	//console.log(value)
	send_req("api/Up",value)
}
function Down(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	//console.log(value)
	send_req("api/Down",value)
}
function Turn_left(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/TLeft",value)
}
function Turn_right(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/TRight",value)
}
function Left(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/Left",value)
}
function Right(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/Right",value)
}
function Backward(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/BW",value)
}
function Forward(value = 0.2){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	console.log(value)
	send_req("api/FW",value)
}



function Flip_forward(){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/FlipF")
}

function Flip_left(){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/FlipL")
}

function Flip_right(){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/FlipR")
}

function Flip_backward(){
	// code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	send_req("api/FlipB")
}


//gamepad
var start;
var a = 0;
var b = 0;

window.addEventListener("gamepadconnected", function(e) {
	console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",e.gamepad.index, e.gamepad.id,e.gamepad.buttons.length, e.gamepad.axes.length)
	gameLoop()
})

window.addEventListener("gamepaddisconnected", function(e) {
	cancelRequestAnimationFrame(start)
})

var interval

if (!('ongamepadconnected' in window)) {
	// No gamepad events available, poll instead.
	interval = setInterval(pollGamepads, 500)
}

function pollGamepads() {
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : [])
	for (var i = 0; i < gamepads.length; i++) {
		var gp = gamepads[i];
		if (gp) {
			console.log("Gamepad connected at index " + gp.index + ": " + gp.id +". It has " + gp.buttons.length + " buttons and " + gp.axes.length + " axes.")
			gameLoop()
			clearInterval(interval)
		}
	}
}

function buttonPressed(button) {
	if (typeof(button) == "object") {
		return button.pressed
	}
	return button >= 0.2 || button <= -0.2
}

function gameLoop() {
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : [])
	if (!gamepads) {
		return;
	}

	var gp = gamepads[0];
	if (buttonPressed(gp.buttons[0])) {
		//button X
		console.log("X")
		Land()
	}
	if (buttonPressed(gp.buttons[1])) {
		//button circle
		console.log("O")
	}
	if (buttonPressed(gp.buttons[2])) {
		//button triangle
		console.log("Tri")
		Take_off()
	}
	if (buttonPressed(gp.buttons[3])) {
		//button square
		console.log("Square")
	}
	if (buttonPressed(gp.buttons[4])) {
		//button L1
		console.log("L1")
	}
	if (buttonPressed(gp.buttons[5])) {
		//button R1
		console.log("R1")
	}

	if (buttonPressed(gp.buttons[6])) {
		//button L2 trigger
		console.log("L2 trigger :"+ gp.buttons[6])
		//console.log(gp.buttons[6])
		//console.log(gp)
	}
	if (buttonPressed(gp.buttons[7])) {
		//button R2 trigger
		console.log("R2 trigger")
	}
	if (buttonPressed(gp.buttons[8])) {
		//button Share
		console.log("Share")
	}
	if (buttonPressed(gp.buttons[9])) {
		//button Options
		console.log("Options")
	}
	if (buttonPressed(gp.buttons[10])) {
		//button PS
		console.log("PS")
	}
	if (buttonPressed(gp.buttons[11])) {
		//button LT
		console.log("Left J button")
	}
	if (buttonPressed(gp.buttons[12])) {
		//button LT
		console.log("Right J button")
	}

	//axes
	if (buttonPressed(gp.axes[0])) {
		//Left Horizontal
		//console.log("L H")
		var value = gp.axes[0]
		if(value>0){
			//min val button 0.2
			Turn_right(0.2+(value-0.2)/4)
		} else {
			Turn_left(0.2-(value+0.2)/4)
		}
	}

	if (buttonPressed(gp.axes[1])) {
		//Left Vertical
		//console.log("L V")
		var value = gp.axes[1]
		if(value>0){
			//min val button 0.2
			Forward(0.2+(value-0.2)/4)
		} else {
			Backward(0.2-(value+0.2)/4)
		}
	}

	if (buttonPressed(gp.axes[2])) {
		//Left Trigger  //non si azzera???
		//console.log("LT trigger %")
	}

	if (buttonPressed(gp.axes[3])) {
		//Right Horizontal
		//console.log("R H")
		var value = gp.axes[3]
		if(value>0){
			//min val button 0.2
			Right(0.2+(value-0.2)/4)
		} else {
			Left(0.2-(value+0.2)/4)
		}
	}
	if (buttonPressed(gp.axes[4])) {
		//Right Vertical
		//console.log("R V")
		var value = gp.axes[4]
		if(value>0){
			//min val button 0.2
			Up(0.2+(value-0.2)/4)
		} else {
			Down(0.2-(value+0.2)/4)
		}
	}
	if (buttonPressed(gp.axes[5])) {
		//Right trigger //non si azzera???
		//console.log("RT trigger %")
	}

	if (buttonPressed(gp.axes[6])) {
		//Dpad H
	}
	if (buttonPressed(gp.axes[7])) {
		//Dpad V
	}


	//gp.hapticActuators
	start = requestAnimationFrame(gameLoop);
}




